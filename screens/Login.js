import React, { useState } from "react";
import {
    Button,
    StyleSheet,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    View
} from "react-native";

export default function Login(props) {
    const [studentId, setStudentId] = useState("");
    const [password, setPassword] = useState("");
    const [schoolId, setSchoolId] = useState("");

    function login() {
        props.login(studentId, password, schoolId);
    }

    return (
        <View style={styles.container}>
            <View style={styles.titleContainer}>
                <Text style={styles.title}>Welcome to Better Kréta</Text>
            </View>
            <View style={styles.inputContainer}>
                <View style={styles.what}>
                    <Text style={styles.whatText}>
                        Log in using your Kréta credentials
                    </Text>
                </View>
                <TextInput
                    style={styles.input}
                    placeholder="Student ID"
                    onChangeText={text => setStudentId(text)}
                ></TextInput>
                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    onChangeText={text => setPassword(text)}
                ></TextInput>
                <TextInput
                    style={styles.input}
                    placeholder="School ID"
                    onChangeText={text => setSchoolId(text)}
                ></TextInput>
                <TouchableHighlight style={styles.button} onPress={login}>
                    <Text style={styles.buttonText}>Login</Text>
                </TouchableHighlight>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    title: {
        color: "#fff",
        fontSize: 26,
        margin: 12,
        marginTop: 24
    },
    container: {
        flex: 1,
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    },
    titleContainer: {
        // backgroundColor: "#242424",
        borderRadius: 24,
        margin: 12
    },
    inputContainer: {
        flex: 1,
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "flex-end",
        backgroundColor: "#242424",
        margin: 12,
        marginBottom: 26,
        borderRadius: 24,
        minWidth: "85%"
    },
    input: {
        backgroundColor: "#444444",
        borderRadius: 24,
        minWidth: "80%",
        height: 40,
        margin: 10,
        textAlign: "center"
    },
    button: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#949494",
        borderRadius: 24,
        width: 100,
        height: 50,
        margin: 20,
        marginBottom: "20%"
    },
    buttonText: {
        fontSize: 16
    },
    what: {
        flex: 1,
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    },
    whatText: {
        color: "#fff",
        fontSize: 32,
        fontWeight: "bold",
        margin: 12,
        textAlign: "center"
    }
});
