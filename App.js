import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Login from "./screens/Login";
import { login } from "./src/Api";

export default function App() {
    return (
        <View style={styles.container}>
            <Content></Content>
        </View>
    );
}

function Content() {
    if (true) return <Login login={login}></Login>;
    else return <Text>content content content</Text>;
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#141414",
        alignItems: "center",
        justifyContent: "center"
    }
});
