import { KretaApi } from "kreta-api";

const api = new KretaApi("guess what");

export async function login(studentId, password, schoolId) {
    await api.authenticate(studentId, password, schoolId);
}
